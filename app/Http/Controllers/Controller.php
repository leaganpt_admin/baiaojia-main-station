<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

    //接口返回格式
    public function toJson($code=0,$msg='',$data=[],$total=0)
    {
        $json = [
            'code'=>$code,
            'msg'=>$msg?$msg:'成功',
        ];
        if ($data) {
            $json['data'] = $data;
            $json['total'] = $total;
        }
        return json_encode($json,JSON_UNESCAPED_UNICODE);
    }

}
