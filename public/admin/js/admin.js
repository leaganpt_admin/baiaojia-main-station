/** 
 * 自定义JS
 */


var domain = '/api/admin/'; //接口前缀
var cacheLogin = 'adminLogin'; //登录缓存


//获取登录信息
function getLoginInfo(){
    return getCache(cacheLogin);
}


//设置登录信息
function setLoginInfo(e){
    return setCache({key:cacheLogin,val:e.val});
}


//退出登录
function setLogout(){
    return delCache({key:cacheLogin});
}


//获取地址栏中参数
function getParamByUrl(name){
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r==null) return '';
     return  unescape(r[2]); 
}


//显示指定字符数
function getStrByNum(e){
    var str = e.str, num = e.num;
    if (str.length<num) {
        return str;
    } else {
        return str.substring(0,num-1)+'...';
    }
}


//自定义弹窗
function pop(e){
    var msg = e.msg, t = e.t;
    t = t ? t*1000 : 1000;
    var h = '';
    h += '<div style="position:fixed;top:0;left:0;z-index:10000;">';
    h += '<div style="width:100%;height:100%;min-height:500px;background:#000;opacity:0.2;position:fixed;top:0;left:0;z-index:10000;"></div>';
    h += '<div style="padding:10px 20px;min-width:100px;max-width:300px;text-align:center;background:#000;color:#FFF;border-radius:5px;position:fixed;bottom:40%;left:40%;z-index:10000;">'+msg+'</div>';
    h += '</div>';
    h += '<script>setTimeout(function(){$("#pop").html("");},'+t+')</script>';
    $('#pop').html(h);
}


//设置缓存
function setCache(e){
    var key = e.key, val = JSON.stringify(e.val);
    window.localStorage.setItem(key, val);
}


//获取缓存
function getCache(key){
    var cache = window.localStorage.getItem(key);
    if (!cache) { return ''; }
    return JSON.parse(cache);
}


//删除缓存
function delCache(e){
    var key = e.key;
    window.localStorage.removeItem(key);
}


//删除缓存
function clearCache(){
    window.localStorage.clear();
}