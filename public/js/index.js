/**
 * 自定义方法
 * */


var domain = ''; //接口前缀口


//自定义弹窗
function pop(e){
    var msg = e.msg;
    var h = '';
    h += '<div style="position:fixed;top:0;left:0;z-index:10000;">';
    //h += '<div style="width:100%;height:100%;min-height:500px;background:#000;opacity:0.2;position:fixed;top:0;left:0;z-index:10000;"></div>';
    h += '<div style="padding:10px 20px;min-width:100px;max-width:300px;text-align:center;background:#000;color:#FFF;border-radius:5px;border:1px solid #333;box-shadow:0 0 5px #333;position:fixed;bottom:40%;left:40%;z-index:10000;">'+msg+'</div>';
    h += '</div>';
    h += '<script>setTimeout(function(){$("#pop").html("");},1000)</script>';
    $('#pop').html(h);
}


//设置缓存
function setCache(e){
    var key = e.key, val = JSON.stringify(e.val);
    window.localStorage.setItem(key, val);
}


//获取缓存
function getCache(key){
    var cache = window.localStorage.getItem(key);
    if (!cache) { return ''; }
    return JSON.parse(cache);
}


//删除缓存
function delCache(e){
    var key = e.key;
    window.localStorage.removeItem(key);
}


//删除缓存
function clearCache(){
    window.localStorage.clear();
}