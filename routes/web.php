<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    //return $router->app->version();
    return redirect('/index.html');
});

//后台接口
$router->get('admin',function(){ return redirect('/admin/index.html'); });
$router->group(['prefix'=>'api/admin','namespace'=>'Admin'], function () use ($router) {
    $router->post('dologin','LoginController@dologin');
});
